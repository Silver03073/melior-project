﻿using UnityEngine.SceneManagement;

public class Controller : Element 
{
    public void GoToScene()
    {
        switch (app.model.sceneID)
        {
            case "StaticSend_1":     
                SceneManager.LoadScene("Static");   
            break;
            
            case "StaticSend_2":
                SceneManager.LoadScene("Static");            
            break;
            
            case "NonStaticSend_1":          
                SceneManager.LoadScene("NonStatic");   
            break;
            
            case "NonStaticSend_2":
                SceneManager.LoadScene("NonStatic");            
            break;
                        
        }
    }
    
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    
    public void InitParameter()
    {
        switch (app.model.sceneID)
        {
            case "StaticSend_1":     
                app.view.receiveValue.Value = Model.staticParameter_1.ToString();   
            break;
            
            case "StaticSend_2":
                app.view.receiveValue.Value = Model.staticParameter_2.ToString();            
            break;
            
            case "NonStaticSend_1":          
                app.view.receiveValue.Value = app.model.nonStaticParameter_1.ToString();   
            break;
            
            case "NonStaticSend_2":
                app.view.receiveValue.Value = app.model.nonStaticParameter_2.ToString();            
            break;
                        
        }
    }
}
