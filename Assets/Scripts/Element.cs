﻿using UnityEngine;

/* Helper base class, giving the child access 
to the instance MVC MeliorApplication */
public class Element : MonoBehaviour 
{
    protected MeliorApplication app { get { return GameObject.FindObjectOfType<MeliorApplication>(); }} 
}
