﻿using UnityEngine;
using UnityEngine.UI;

public class ReceiveValue : Element 
{
    [HideInInspector]
    public string Value { get { return textComponent.text; } set { textComponent.text = value; } }
    private Text textComponent;
    
    void Awake()
    {
        textComponent = GetComponent<Text>();
        app.view.receiveValue = this;        
    }
    
    void Start()
    {
        app.controller.InitParameter();
    }
}
