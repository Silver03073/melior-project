﻿using UnityEngine;
using System.Collections;

/* The main class, which have only one instance that contains 
links to all of the game elements that are instantiated */

public class MeliorApplication : MonoBehaviour 
{
    public static MeliorApplication Instance;
    public Model model;
    public View view;
    public Controller controller;
    
    void Awake()
    {
        // Singleton decalaration
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
    }
}
