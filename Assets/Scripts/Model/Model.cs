﻿using UnityEngine;

public class Model : Element 
{    
    public static int staticParameter_1 = 10;
    public static int staticParameter_2 = 20;
    [HideInInspector]
    public int nonStaticParameter_1 = 30;
    [HideInInspector]
    public int nonStaticParameter_2 = 40;     
    [HideInInspector]
    public string sceneID;    
}
